# Import SDK packages
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
import time
import json
import pandas as pd
import numpy as np

class MQTTClient:
    def __init__(self, device_id, cert, key):
        # For certificate based connection
        self.device_id = "CarThing-"+str(device_id)
        self.state = 0
        self.client = AWSIoTMQTTClient(self.device_id)
        # TODO 2: modify your broker address
        self.client.configureEndpoint("a1epsmsmelgrz7-ats.iot.us-east-1.amazonaws.com", 8883)
        self.client.configureCredentials("./certificates/AmazonRootCA1.pem", key, cert)
        self.client.configureOfflinePublishQueueing(-1)  # Infinite offline Publish queueing
        self.client.configureDrainingFrequency(2)  # Draining: 2 Hz
        self.client.configureConnectDisconnectTimeout(10)  # 10 sec
        self.client.configureMQTTOperationTimeout(5)  # 5 sec
        self.client.onMessage = self.customOnMessage

    def customOnMessage(self, message):
        # TODO3: fill in the function to show your received message
        print("client {} received payload {} from topic {}".format(self.device_id, message.payload, message.topic))

    # Suback callback
    def customSubackCallback(self, mid, data):
        # You don't need to write anything here
        pass

    # Puback callback
    def customPubackCallback(self, mid):
        # You don't need to write anything here
        pass

    def publish(self, Payload="payload"):
        # TODO4: fill in this function for your publish
        self.client.subscribeAsync("CarTopic", 0, ackCallback=self.customSubackCallback)

        #self.client.publishAsync("CarTopic", Payload, 0, ackCallback=self.customPubackCallback)

    def subscribe(self):
        self.client.subscribeAsync("CarTopic", 0, ackCallback=self.customSubackCallback)

def main():
    # TODO 1: modify the following parameters
    # Starting and end index, modify this
    device_st = 1
    device_end = 2

    # Path to the dataset, modify this
    data_path = "data2/vehicle{}.csv"

    # Path to your certificates, modify this
    certificate_formatter = "./certificates/vehicle{}/vehicle{}.pem"
    key_formatter = "./certificates/vehicle{}/vehicle{}.private.pem"

    print("Loading vehicle data...")
    data = []
    for i in range(5):
        a = pd.read_csv(data_path.format(i))
        data.append(a)

    print("Initializing MQTTClients...")
    clients = []
    for device_id in range(device_st, device_end):
        client = MQTTClient(device_id, certificate_formatter.format(device_id, device_id),
                            key_formatter.format(device_id, device_id))
        client.client.connect()
        clients.append(client)

    c=clients[0]

    print("Receiving packets")
    c.subscribe()
    while True:
        pass
        # time.sleep(3)

if __name__ == '__main__':
    main()
