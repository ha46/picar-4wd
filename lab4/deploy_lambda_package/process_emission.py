import json
import logging
import sys
import collections
import greengrasssdk

# Logging
logger = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

# SDK Client
client = greengrasssdk.client("iot-data")

# Counter
my_counter = 0

car_co2_tracker = collections.defaultdict(int)


def lambda_handler(event, context):
    global my_counter
    global car_co2_tracker

    # TODO1: Get your data

    # event = {
    #     "co2_emission" : 3,
    #     "vehicle_id": 1
    # }

    # TODO2: Calculate max CO2 emission
    car_co2_tracker[event['vehicle_id']] = max(car_co2_tracker[event['vehicle_id']], event['co2_emission'])

    # TODO3: Return the result
    client.publish(
        topic="max_co2_emission/" + str(event['vehicle_id']),
        payload=json.dumps(
            {"message": "Sent from Greengrass Core.  Invocation Count: {}   {}".format(my_counter, car_co2_tracker[
                event['vehicle_id']])}
        ),
    )

    client.publish(
        topic="co2_data",
        payload=json.dumps(
            {"co2_emission": event["co2_emission"],
             "vehicle_id": event["vehicle_id"],
             "timestamp": event["timestamp"],
             "vehicle_speed": event["vehicle_speed"],
             "vehicle_fuel": event["vehicle_fuel"]
             }),
    )
    my_counter += 1

    return
