import os
import sys
import time
import uuid
import json
import logging
import argparse
import csv
from AWSIoTPythonSDK.core.greengrass.discovery.providers import DiscoveryInfoProvider
from AWSIoTPythonSDK.core.protocol.connection.cores import ProgressiveBackOffCore
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
from AWSIoTPythonSDK.exception.AWSIoTExceptions import DiscoveryInvalidRequestException

AllowedActions = ['both', 'publish', 'subscribe']


# General message notification callback
def get_max_co2_emissions(message):
    print('Received max emission as %s on topic %s: \n' % (message.topic, message.payload))


MAX_DISCOVERY_RETRIES = 10
GROUP_CA_PATH = "./groupCA/"

# Read in command-line parameters
parser = argparse.ArgumentParser()

parser.add_argument("-t", "--vehicleId", action="store", dest="vehicleId", default="0", help="Id of vehicle")

parser.add_argument("-p", "--print_discover_resp_only", action="store_true", dest="print_only", default=False)

args = parser.parse_args()
host = "a1cg9t2t21qd4x-ats.iot.us-east-1.amazonaws.com"
rootCAPath = f"./certificates/AmazonRootCA1.pem"
vehicleId = args.vehicleId
certificatePath = f"./certificates/vehicle{vehicleId}/vehicle{vehicleId}.pem"
privateKeyPath = f"./certificates/vehicle{vehicleId}/vehicle{vehicleId}.private.pem"
clientId = f"CarThing-{vehicleId}"
thingName = f"CarThing-{vehicleId}"
pubTopic = f"co2_emissions"
subTopic = f"max_co2_emission/{vehicleId}"
print_only = args.print_only

if not os.path.isfile(rootCAPath):
    parser.error("Root CA path does not exist {}".format(rootCAPath))
    exit(3)

if not os.path.isfile(certificatePath):
    parser.error("No certificate found at {}".format(certificatePath))
    exit(3)

if not os.path.isfile(privateKeyPath):
    parser.error("No private key found at {}".format(privateKeyPath))
    exit(3)

# Configure logging
logger = logging.getLogger("AWSIoTPythonSDK.core")
logger.setLevel(logging.INFO)
# streamHandler = logging.StreamHandler()
# formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
# streamHandler.setFormatter(formatter)
# logger.addHandler(streamHandler)

# Progressive back off core
backOffCore = ProgressiveBackOffCore()

# Discover GGCs
discoveryInfoProvider = DiscoveryInfoProvider()
discoveryInfoProvider.configureEndpoint(host)
discoveryInfoProvider.configureCredentials(rootCAPath, certificatePath, privateKeyPath)
discoveryInfoProvider.configureTimeout(10)  # 10 sec

retryCount = MAX_DISCOVERY_RETRIES if not print_only else 1
discovered = False
groupCA = None
coreInfo = None
while retryCount != 0:
    try:
        discoveryInfo = discoveryInfoProvider.discover(thingName)
        caList = discoveryInfo.getAllCas()
        coreList = discoveryInfo.getAllCores()

        # We only pick the first ca and core info
        groupId, ca = caList[0]
        coreInfo = coreList[0]
        print("Discovered GGC: %s from Group: %s" % (coreInfo.coreThingArn, groupId))

        print("Now we persist the connectivity/identity information...")
        groupCA = GROUP_CA_PATH + groupId + "_CA_" + str(uuid.uuid4()) + ".crt"
        if not os.path.exists(GROUP_CA_PATH):
            os.makedirs(GROUP_CA_PATH)
        groupCAFile = open(groupCA, "w")
        groupCAFile.write(ca)
        groupCAFile.close()

        discovered = True
        print("Now proceed to the connecting flow...")
        break
    except DiscoveryInvalidRequestException as e:
        print("Invalid discovery request detected!")
        print("Type: %s" % str(type(e)))
        print("Error message: %s" % str(e))
        print("Stopping...")
        break
    except BaseException as e:
        print("Error in discovery!")
        print("Type: %s" % str(type(e)))
        print("Error message: %s" % str(e))
        retryCount -= 1
        print("\n%d/%d retries left\n" % (retryCount, MAX_DISCOVERY_RETRIES))
        print("Backing off...\n")
        backOffCore.backOff()

if not discovered:
    # With print_discover_resp_only flag, we only woud like to check if the API get called correctly.
    if print_only:
        sys.exit(0)
    print("Discovery failed after %d retries. Exiting...\n" % (MAX_DISCOVERY_RETRIES))
    sys.exit(-1)

# Iterate through all connection options for the core and use the first successful one
myAWSIoTMQTTClient = AWSIoTMQTTClient(clientId)
myAWSIoTMQTTClient.configureCredentials(groupCA, privateKeyPath, certificatePath)
myAWSIoTMQTTClient.onMessage = get_max_co2_emissions

connected = False
for connectivityInfo in coreInfo.connectivityInfoList:
    currentHost = connectivityInfo.host
    currentPort = connectivityInfo.port
    print("Trying to connect to core at %s:%d" % (currentHost, currentPort))
    myAWSIoTMQTTClient.configureEndpoint(currentHost, currentPort)
    try:
        myAWSIoTMQTTClient.connect()
        connected = True
        break
    except BaseException as e:
        print("Error in connect!")
        print("Type: %s" % str(type(e)))
        print("Error message: %s" % str(e))

if not connected:
    print("Cannot connect to core %s. Exiting..." % coreInfo.coreThingArn)
    sys.exit(-2)

# Successfully connected to the core
myAWSIoTMQTTClient.subscribe(subTopic, 0, None)
time.sleep(2)

with open(f"./data2/vehicle{vehicleId}.csv", 'r') as csvfile:
    reader = csv.reader(csvfile)
    next(reader)
    for row in reader:
        # Process the row data
        # print(row)
        message = {}
        message['co2_emission'] = float(row[2])
        message['vehicle_id'] = vehicleId
        message['timestamp'] = int(float(row[0]))
        message['vehicle_speed'] = float(row[15])
        message['vehicle_fuel'] = float(row[9])
        messageJson = json.dumps(message)
        myAWSIoTMQTTClient.publish(pubTopic, messageJson, 0)
        print('Published topic %s: %s\n' % (pubTopic, messageJson))
        time.sleep(1)

# python3 gg_local_publisher.py --endpoint a1epsmsmelgrz7-ats.iot.us-east-1.amazonaws.com --rootCA AmazonRootCA1.pem --cert vehicle0.pem --key vehicle0.private.pem --thingName CarThing-0 --topic 'hello/world/pubsub' --mode publish --message 'Hello, World! Sent from CarThing-0/iotLab'
