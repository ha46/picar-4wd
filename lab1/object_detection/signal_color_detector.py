import numpy as np
import cv2

def detect(img, top_left, bottom_right):
    # img = cv2.imread("/Users/mukeshchugani/Desktop/picar-4wd/sample_images/green.jpeg")
    cv2.imwrite("original.jpg", img)
    img = img[top_left[1]:bottom_right[1], top_left[0]:bottom_right[0]]
    cv2.imwrite("cropped.png", img)

    img = cv2.resize(img, (2464, 3280), interpolation=cv2.INTER_LINEAR)

    img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    lower_red = np.array([176,119,119])
    upper_red = np.array([179,255,255])
    mask_red = cv2.inRange(img_hsv, lower_red, upper_red)

    cv2.imwrite("mask_red.png", mask_red)

    lower_green = np.array([66,0,60])
    upper_green = np.array([123,255,189])
    mask_green = cv2.inRange(img_hsv, lower_green, upper_green)

    cv2.imwrite("mask_green.png", mask_green)

    # set my output img to zero everywhere except my mask
    output_img_green = img.copy()
    output_img_green = cv2.bitwise_and(output_img_green, output_img_green, mask= mask_green)
    output_img_green = cv2.cvtColor(output_img_green, cv2.COLOR_BGR2GRAY)

    output_img_red = img.copy()
    output_img_red = cv2.bitwise_and(output_img_red, output_img_red, mask= mask_red)
    output_img_red = cv2.cvtColor(output_img_red, cv2.COLOR_BGR2GRAY)

    red_pixels = cv2.countNonZero(output_img_red)
    green_pixels = cv2.countNonZero(output_img_green)

    # cv2.imshow("tp", output_img_green)
    # cv2.waitKey(0)
  
    # # # closing all open windows
    # cv2.destroyAllWindows()

    if red_pixels > green_pixels:
        return "red"
    else:
        return "green"
