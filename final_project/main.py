from flask import Flask, request
from pymongo import MongoClient
import firebase_admin
from firebase_admin import credentials
from firebase_admin import messaging
import socket
import time


class TTLDictionary:
    def __init__(self):
        self._dict = {}

    def set(self, key, value):
        self._dict[key] = (value, time.time() + 1)

    def get(self, key):
        value, expire_time = self._dict.get(key, (None, None))
        if expire_time is None or time.time() < expire_time:
            return value
        else:
            del self._dict[key]
            return None


app = Flask(__name__)
host = '192.168.10.98'
port = 5000


@app.route('/')
def hello():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, port))
    s.send(b'exit\n')
    s.close()
    return 'Hello, World!'

@app.route('/init')
def init():
    things = items.find({}, {'_id' : 0})
    results = []
    for thing in things:
        results.append(thing)
        # print(thing)
    return {"results" : results}


@app.route('/item', methods=['POST'])
def detect():
    name = request.form['name']
    direction = request.form['direction']
    image = request.form['image']
    expiry = request.form['expiry']
    print('got',name,direction)
    if itemDictionary.get(name+direction) is None:
        itemDictionary.set(name+direction, image)
        # up is add
        if direction == 'Up':
            item = items.find_one({"name": f'{name}'})
            if item is None:
                data = {'name': f'{name}', 'expiry' : f'{expiry}',
                                  'addDate': f'{int(time.time())}',
                                  'foodImage': f'{image}'}
                items.insert_one(data)
                data['direction'] = direction
                # print(data)
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.connect((host, port))
                s.send(f'{str(data)}\n'.encode())
                # print(len(f'{str(data)}\n'.encode()))
                s.send(b'exit\n')
                s.close()
        else:
            print('searching')
            item = items.find_one({"name": f'{name}'}, {'_id' : 0})
            if item is not None:
                print(item['name'])
                items.delete_one({"name": f'{name}'})
                item['direction'] = direction
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.connect((host, port))
                s.send(f'{str(item)}\n'.encode())
                s.send(b'exit\n')
                # print(len(f'{str(item)}\n'.encode()))
                s.close()
    else:
        print('thesholding')
    return 'Item Added'

@app.route('/notify')
def notify():
    message = messaging.Message(
        notification=messaging.Notification(
            title="Eggs Expiring Soon",
            body=" "
        ),
        token="efdxy6ahr2rBIDiHRqzat6:APA91bG3xmYauZ8I8XLwb3-gYcoifIS7aexxIzneCIrVb0NLoT5-EY7q3rFXAZ_dq8nqQ_G0d8yPtz5pOyE4mmBBu1kVie5aHW1xKLe2GrbY3L8kWqmPZL3FOcn9Vm9g_GJ7tX9Bjr9s"
    )
    response = messaging.send(message)
    return response
    # print("Successfully sent message:", response)

if __name__ == '__main__':
    client = MongoClient('mongodb+srv://cs437:iotfinal@finalproject.n4ilclx.mongodb.net/iot')
    db = client.iot
    items = db.items
    itemDictionary = TTLDictionary()
    cred = credentials.Certificate("/Users/harshit/Desktop/Codes/CS437/gitlab/picar-4wd/final_project/iotsmartfoodinventory-firebase-adminsdk-wkwut-650b1572a6.json")
    firebase_admin.initialize_app(cred)
    app.run(host='0.0.0.0', port=5000, debug=True)