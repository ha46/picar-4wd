document.onkeydown = updateKey;
document.onkeyup = resetKey;
let socket;
// connect();

var server_port = 65432;
var video_stream_port = 5002
var server_addr = "192.168.10.79";   // the IP address of your Raspberry PI
// var server_addr = '127.0.0.1'
var isConnected = false;

function establishConnection()
{
    socket = io(`ws://${server_addr}:${server_port}`);
    socket.on('info', (data) => {
        console.log(data)
    });

    socket.on('connect', (con) => {
        isConnected = true;
        console.log('Connected to server')
        setInterval(function(){
            // get image from python server
            get_info()
        }, 2000)
    })

    socket.on('disconnect', (con) => {
        isConnected = true;
        console.log('Disconnect to server')
    })

    socket.on('info', (data) => {
        document.getElementById("speed").innerHTML = data['speed'];
        document.getElementById("temp").innerHTML = data['temp'];
        document.getElementById("location").innerHTML = data['city'];
    })
}

establishConnection()

function start_video_stream(){
    video_socket = new WebSocket(`ws://${server_addr}:${video_stream_port}/`);
    console.log("executing")
    video_socket.addEventListener('message', (e) => {
        let msg = document.getElementById("msg");
        let ctx = msg.getContext("2d");
        let image = new Image();
        image.src = URL.createObjectURL(e.data);
        image.addEventListener("load", (e) => {
            ctx.drawImage(image, 0, 0, msg.width, msg.height);
        });
    });
}


function get_info(){
    if(isConnected)
    {
        socket.emit('get_info', 'info');
    }
    else
    console.log("Not connected");
}

function sendMessage(){
    var input = document.getElementById("message").value;

    client.write(`${input}\r\n`);

    client.on('end', () => {
        console.log('disconnected from server');
    });


}

function sendDirection(direction)
{
    if(isConnected)
    {
        socket.emit('get_direction', direction);
    }
    else
    console.log("Not connected");
    
}

// for detecting which key is been pressed w,a,s,d
function updateKey(e) {

    e = e || window.event;

    if (e.keyCode == '87') {
        // up (w)
        document.getElementById("upArrow").style.color = "green";
        sendDirection("87");
    }
    else if (e.keyCode == '83') {
        // down (s)
        document.getElementById("downArrow").style.color = "green";
        sendDirection("83");
    }
    else if (e.keyCode == '65') {
        // left (a)
        document.getElementById("leftArrow").style.color = "green";
        sendDirection("65");
    }
    else if (e.keyCode == '68') {
        // right (d)
        document.getElementById("rightArrow").style.color = "green";
        sendDirection("68");
    }
}

// reset the key to the start state 
function resetKey(e) {

    e = e || window.event;
    sendDirection('stop')

    document.getElementById("upArrow").style.color = "grey";
    document.getElementById("downArrow").style.color = "grey";
    document.getElementById("leftArrow").style.color = "grey";
    document.getElementById("rightArrow").style.color = "grey";
}






// function connect()
// {
//     setInterval(function(){
//         // get image from python server
//         if(!isConnected)
//         establishConnection();
//     }, 5000)
// }



// // update data for every 50ms
// function update_data(){
//     sendMessage();
// }


// function openSocket() {

//     socket = new WebSocket(`ws://${server_addr}:${video_stream_port}/`);
//     console.log("executing")
//     socket.addEventListener('open', (e) => {
//         document.getElementById("status").innerHTML = "Opened";
//     });
//     socket.addEventListener('message', (e) => {
//         console.log(e.data)
//         let msg = document.getElementById("msg");
//         let ctx = msg.getContext("2d");
//         let image = new Image();
//         image.src = URL.createObjectURL(e.data);
//         image.addEventListener("load", (e) => {
//             ctx.drawImage(image, 0, 0, msg.width, msg.height);
//         });
//     });
// }

// openSocket()

