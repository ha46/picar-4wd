from bluedot.btcomm import BluetoothServer
from signal import pause
import picar_4wd as fc
import public_ip
import ipinfo
import socket

fc.start_speed_thread()


def received_handler(msg):
    # print("THe msg is", msg)
    if 'info' in msg:
        access_token = 'ecc1adedb2b01e'
        handler = ipinfo.getHandler(access_token)
        # ip_address = str(fc.utils.getIP())
        details = handler.getDetails(public_ip.get())
        # print(details)
        response = str(details.city) +"|"+str(fc.utils.cpu_temperature()) + "|" + str(fc.speed_val())
        # print("The response is ", response)
        s.send(response)

    elif '|' in msg:
        # print("Msg is ", msg.split('|'))
        key, power_val = tuple(msg.split('|'))

        # print("Key pval", key, power_val)
        power_val = int(power_val.strip('\r\n'))

        if key == 'w':
            print("Moving forward")
            fc.forward(power_val)
        elif key == 'a':
            print("Moving left")
            fc.turn_left(power_val)
        elif key == 's':
            print("Moving back")
            fc.backward(power_val)
        elif key == 'd':
            print("Moving right")
            fc.turn_right(power_val)
        else:
            print("Stopping")
            fc.stop()

        s.send("key")

s = BluetoothServer(received_handler)

pause()



# fc.stop()