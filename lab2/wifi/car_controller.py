import picar_4wd as fc
import public_ip
import ipinfo
import eventlet
import socketio



access_token = 'ecc1adedb2b01e'
handler = ipinfo.getHandler(access_token)
ip = public_ip.get()

server_io = socketio.Server(cors_allowed_origins = '*')
app = socketio.WSGIApp(server_io)

fc.start_speed_thread()

# Triggered when a client connects to our socket. 
@server_io.event
def connect(sid, socket):    
    print(sid, 'connected')

# Triggered when a client disconnects from our socket
@server_io.event
def disconnect(sid):
    print(sid, 'disconnected')

@server_io.event
def get_info(sid, data):
    print("got info request")
    details = handler.getDetails(ip)
    # value = str(details.city) +"|"+str(fc.utils.cpu_temperature()) + "|" + str(fc.speed_val())
    value = 'test'
    server_io.emit("info", {'city': str(details.city), 'temp' : str(fc.utils.cpu_temperature()), 'speed' : str(fc.speed_val()) }, to=sid)
    # server_io.emit("info", {'city': 'champaign', 'temp' : '12', 'speed' : '34' }, to=sid)

@server_io.event
def get_direction(sid,DATA):
    if DATA == "87":
        # print('forward')
        fc.forward(10) 
    elif DATA == "83":
        fc.backward(10)
        # print('back')
    elif DATA == "65":
        fc.turn_left(10)
        # print('left')
    elif DATA == "68":
        fc.turn_right(10)
        # print('right')
    elif DATA == "stop":
        fc.stop()
        # print('stop')


eventlet.wsgi.server(eventlet.listen(('0.0.0.0', 65432)), app)



