# WS server that sends camera streams to a web server using opencv

import asyncio
import websockets
import cv2

# vcgencmd get_camera good command to check pi camera

async def time(websocket, path):
    while True:
        vid = cv2.VideoCapture(0)
        # vid = cv2.VideoCapture('test.mp4')
        try:
            while (vid.isOpened()):
                img, frame = vid.read()

                frame = cv2.resize(frame, (640, 480))
                encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 65]
                man = cv2.imencode('.jpg', frame, encode_param)[1]
                # print("Sending image", frame.shape)
                await websocket.send(man.tobytes())
            # print("Connected")
            # await websocket.send(b'hi harshit')
        except:
            vid.close()
            pass



if __name__ == '__main__':
    start_server = websockets.serve(time, "0.0.0.0", 5002)
    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()

